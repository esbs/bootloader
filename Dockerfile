# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

FROM index.docker.io/library/alpine:latest

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN \
    apk add --no-cache \
        binutils-arm-none-eabi \
        bison \
        bzip2 \
        dtc \
        flex \
        git \
        gzip \
        make \
        musl-dev \
        ncurses-dev \
        openssl-dev \
        perl \
        swig \
        xz \
        zstd \
    && \
    apk add \
        --no-cache \
        --repository="$(sed '1 s|^\(.*alpine/\)\(.*\)$|\1edge/community|;2,$d' "/etc/apk/repositories")" \
        gcc-arm-none-eabi \
    && \
    apk add \
        --no-cache \
        --repository="$(sed '1 s|^\(.*alpine/\)\(.*\)$|\1edge/testing|;2,$d' "/etc/apk/repositories")" \
        lzop \
    && \
    rm -rf "/var/cache/apk/"*

ENV TARGET_ARCH="arm"
ENV CROSS_COMPILE="arm-none-eabi-"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
