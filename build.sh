#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_BUILD_SRC="./u-boot"
DEF_KEEP_BUILD_CACHE="false"
DEF_OUTPUT_DIR="output"
PREFIX="${PREFIX:-/usr}"


usage()
{
	echo "Uage: ${0} [OPTIONS] [COMMAND]"
	echo
	echo "This is a wrapper script around the normal U-Boot Makefile, calling make with [COMMAND]."
	echo "Its purpose is to make setting of build dirs and passing of configurations easier."
	echo "    -a  Set the target architecture, an empty var performs a native build (current: '${TARGET_ARCH:-native}'). [TARGET_ARCH]"
	echo "    -b  Build config directory to use [BUILD_CONFIG]"
	echo "    -c  Use previous build cache (default: ${DEF_KEEP_BUILD_CACHE}) [KEEP_BUILD_CACHE]"
	echo "    -h  Print usage"
	echo "    -o  Set the output directory (default: '${DEF_OUTPUT_DIR}/\${PREFIX:-${PREFIX}}/') [OUTPUT_DIR]"
	echo "    -s  Location of the U-Boot source directory (default: '${DEF_BUILD_SRC}') [BUILD_SRC]"
	echo "    -x  Override the target compiler, an empty var performs a native build (default: native). [CROSS_COMPILE]"
	echo
	echo "All options can also be passed in environment variables (listed between [BRACKETS])."
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg "err" "${*}" >&2
}

e_warn()
{
	_msg "warning" "${*}"
}

e_notice()
{
	_msg "notice" "${*}"
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	if [ -z "${MAKEFLAGS:-}" ]; then
		MAKEFLAGS="-j$(($(nproc) - 1))"
		e_warn "MAKEFLAGS where not set, setting them to '${MAKEFLAGS}'."
		export MAKEFLAGS
	fi

	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -f -r "${build_dir:?}"
	fi
	mkdir -p "${build_dir}"

	if [ -z "${LOCALVERSION:-}" ] && \
	   git rev-parse --show-cdup 2> "/dev/null"; then
		if ! git describe 2> "/dev/null"; then
			LOCALVERSION="g"
		fi
		LOCALVERSION="-${LOCALVERSION:-}$(git describe --always --dirty)"
		export LOCALVERSION
	fi

	if [ -n "${output_dir##/*}" ]; then
		output_dir="$(pwd)/${output_dir}"
	fi
	if [ -d "${output_dir}" ]; then
		e_notice "Existing output directory '${output_dir}' being re-used"
	fi

	if [ -f "${build_config}/u-boot.config" ]; then
		uboot_config="$(readlink -f "${build_config}/u-boot.config")"
	else
		if [ -n "${build_config##/*}" ]; then
			_pwd="$(pwd)/"
		fi
		uboot_config="${_pwd:-}${build_config}/u-boot.config"
		return
	fi
	# The following code requires a valid config and so should not be used
	# in other cases, e.g. when creating a new config via defconfig.

	if [ ! -f "${build_dir}/include/config/auto.conf" ]; then
		build_uboot "prepare"
	fi
	uboot_release="$(build_uboot -s "ubootrelease")"
}

cleanup()
{
	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -f -r "${build_dir:?}"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

build_uboot()
{
	ARCH="${arch}" \
	CROSS_COMPILE="${cross_compile}" \
	nice -n 19 \
	     make -C "${build_src}" \
	     DESTDIR="${output_dir}" \
	     INSTALL_PATH="${output_dir}/${install_path}" \
	     KCONFIG_CONFIG="${uboot_config}" \
	     O="${build_dir}" \
	     PREFIX="${PREFIX}" \
	     "${@}"
}

main()
{
	_start_time="$(date "+%s")"

	while getopts ":a:b:cho:s:x:" _options; do
		case "${_options}" in
		a)
			arch="${OPTARG}"
			;;
		b)
			build_config="$(echo "${OPTARG}" | sed 's|/*$||g')"
			;;
		c)
			keep_build_cache="true"
			;;
		h)
			usage
			exit 0
			;;
		o)
			output_dir="${OPTARG}"
			;;
		s)
			build_src="${OPTARG}"
			;;
		x)
			cross_compile="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${build_config:=${BUILD_CONFIG:-}}" ]; then
		for config in "configs/"*; do
			if [ -d "${config}" ] && \
			   [ -f "${config}/u-boot.config" ]; then
				build_config="${config}"
				break
			fi
		done
	fi

	arch="${arch:-${TARGET_ARCH:-$(uname -m)}}"
	build_name="${build_config##*/}"
	build_dir="${BUILD_DIR:-$(pwd)/.build/${arch}/${build_name}}"
	build_src="$(readlink -f "${build_src:-${BUILD_SRC:-${DEF_BUILD_SRC}}}")"
	cross_compile="${cross_compile:-${CROSS_COMPILE:-}}"
	install_path="${PREFIX}/share/u-boot/${build_name}"
	keep_build_cache="${keep_build_cache:-${KEEP_BUILD_CACHE:-${DEF_KEEP_BUILD_CACHE}}}"
	output_dir="${output_dir:-${OUTPUT_DIR:-$(pwd)/${DEF_OUTPUT_DIR}}}"

	if [ ! -f "${build_config}/u-boot.config" ]; then
		e_warn "Missing build configuration '${build_config}'."
		printf "Create new one? (y/n): "
		read -r __sure
		echo
		if [ "${__sure}" != "Y" ] && \
		   [ "${__sure}" != "y" ]; then
			e_err "Cannot continue without configuration, aborting."
			exit 1
		fi

		mkdir -p "${build_config}"
	fi

	init

	case "${@}" in
	all*)
		# Rely on the internal scripts 'all' -> install
		shift "${#}"
		;;
	*_defconfig)
		build_uboot "${@}"
		exit 0
		;;
	*help*)
		build_uboot "help"
		cleanup
		exit 0
		;;
	*menuconfig*)
		build_uboot "menuconfig"
		cleanup
		exit 0
		;;
	*)
		;;
	esac

	echo "Building for target '${build_name}', '${uboot_release:=unknown}'."
	if [ "${#}" -gt 0 ]; then
		build_uboot "${@}"
	else
		echo "Installing into '${output_dir}'."
		build_uboot install
	fi

	echo "==============================================================================="
	echo "Build report for $(date -u)"
	echo "Version: U-Boot ${uboot_release}"
	echo
	if [ -f "${output_dir}/${install_path}/u-boot-spl.bin" ]; then
		echo "U-Boot SPL size:		$(du -d 0 -h "$(readlink -f "${output_dir}/${install_path}/u-boot-spl.bin")" | cut -f1)"
	fi
	if [ -f "${output_dir}/${install_path}/u-boot.img" ]; then
		echo "U-Boot proper size:	$(du -d 0 -h "$(readlink -f "${output_dir}/${install_path}/u-boot.img")" | cut -f1)"
	fi
	echo
	echo "Successfully built U-Boot for '${build_name}' in $(($(date "+%s") - _start_time)) seconds."
	echo "==============================================================================="

	cleanup
}

main "${@}"

exit 0
