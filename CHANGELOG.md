# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


**NOTE:** DO NOT EDIT! This changelog is automatically generated. See the README.md file for more information.

## [v0.4.0] - 2021-02-12
### Changed
- Update container to work with alpine:v3.13
- Do not remove the output directory

## [v0.3.0] - 2020-12-03
### Changed
- Major scripts cleanup

## [v0.2.0] - 2020-09-21
### Changed
- Switch to Alpine Linux based containers

## [v0.1.0] - 2020-01-23
### Added
- Initial build setup
